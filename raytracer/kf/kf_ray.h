#ifndef KF_RAY_HEADER
#define KF_RAY_HEADER

#include "kf/kf_vector3.h"
#include <ostream>

namespace kf
{
   template<typename VT>
   class RayT
   {
   public:
      typedef VT type;

      __forceinline RayT()
      {
         m_length = 0;
      }
      __forceinline RayT(const VT &start): m_start(start)
      {
         m_length = 0;
      }
      __forceinline RayT(const VT &start, const VT &delta): m_start(start), m_delta(delta)
      {
         m_length = delta.length();
      }

      __forceinline friend std::ostream &operator<<(std::ostream &o, const RayT &r)
      {
         o << "[" << r.m_start << ":" << r.m_delta + r.m_start << "]";
         return o;
      }

      __forceinline typename VT::type length() const
      {
         return m_length;
      }

      __forceinline void length(typename VT::type value)
      {
         if(m_length!=0)
         {
            m_delta *= value / m_length;
            m_length = m_delta.length();
         }
      }

      __forceinline VT start() const
      {
         return m_start;
      }

      __forceinline void start(const VT &v)
      {
         m_start = v;
      }

      __forceinline VT end() const
      {
         return m_start+m_delta;
      }

      __forceinline void end(const VT &v)
      {
         m_delta = v-m_start;
         m_length = m_delta.length();
      }

      __forceinline VT delta() const
      {
         return m_delta;
      }

      __forceinline void delta(const VT &v)
      {
         m_delta = v;
         m_length = m_delta.length();
      }

      __forceinline VT interpolate(const typename VT::type &value) const
      {
         return m_start + m_delta * value;
      }

      __forceinline bool intersectSphere(const kf::Vector3 &pos, float radius, float& distance) const
      {
          kf::Vector3 L = pos - m_start;
          float tca = L.dot(m_delta);
          if (tca < 0) return false;
          float d2 = L.dot(L) - tca * tca;
          if (d2 > radius * radius)
          {
              distance = 0;
              return false;
          }
          float thc = sqrt(radius * radius - d2);
          float t0 = tca - thc;
          float t1 = tca + thc;
          distance = std::min(t0, t1);
          return true;
      }

      __forceinline bool intersectBox(const kf::Vector3 &pos, const kf::Vector3 &size, float& distance) const
      {
          kf::Vector3 minCorner(pos - size * 0.5);
          kf::Vector3 maxCorner(pos + size * 0.5);

          kf::Vector3 inv = 1.0f / m_delta;

          double t1 = (minCorner.x - m_start.x) * inv.x;
          double t2 = (maxCorner.x - m_start.x) * inv.x;
          double tmin = std::min(t1, t2);
          double tmax = std::max(t1, t2);
          for (int i = 1; i < 3; ++i)
          {
              t1 = (minCorner[i] - m_start[i]) * inv[i];
              t2 = (maxCorner[i] - m_start[i]) * inv[i];
              tmin = std::max(tmin, std::min(std::min(t1, t2), tmax));
              tmax = std::min(tmax, std::max(std::max(t1, t2), tmin));
          }
          if (tmax > std::max(tmin, 0.0))
          {
              distance = tmin;
              return true;
          }
          return false;
      }

protected:
      VT m_start;
      VT m_delta;
      typename VT::type m_length;
   };

   typedef RayT<kf::Vector3>  Ray;
}

#endif

