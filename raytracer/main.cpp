#include "CImg.h"
#include "kf/kf_vector.h"
#include "scene.h"
#include "renderable.h"
#include "kf/kf_ray.h"
#include "light.h"
#include "windows.h"
#include "luascript.h"
#include "kf/kf_time.h"
#include "kf/kf_algorithms.h"
#include "kf/kf_math.h"
#include <iostream>
#include "Poco/Environment.h"
#include "Poco/Thread.h"

using namespace cimg_library;

// This controls if the rendering displays progressively. When timing the results, turn this off (otherwise updating the window repeatedly will affect the timing)
// progressiveCount is how many lines are rendered before updating the window.
bool progressiveDisplay = false;
int  progressiveCount   = 10;

// The resolution of the window and the output of the ray tracer. This can be overridden by the Lua startup script.
int windowWidth = 1024;
int windowHeight = 1024;

CImg<float> image(windowWidth, windowHeight, 1, 3, 0);
CImgDisplay main_disp;//(image, "Raytrace");

Poco::Mutex mutex;
int rayLine = 0;
bool running = true;

int threadsRun = 0;

// The scene object.
Scene g_scene;

// Lua state object used to run the startup script.
lua_State *g_state;

void RayTrace(void* data)
{
	while (running)
	{
		mutex.lock();
			
		if (rayLine >= windowHeight)
		{
			running = false;
			mutex.unlock();
		}
		else
		{			
			int row = rayLine;
			rayLine++;
			mutex.unlock();
			
			for (int x = 0; x < windowWidth; x++)
			{
				// Retrieve the colour of the specified pixel. The math below converts pixel coordinates (0 to width) into camera coordinates (-1 to 1).
				kf::Colour output = g_scene.trace(float(x - windowWidth / 2) / (windowWidth / 2), -float(row - windowHeight / 2) / (windowHeight / 2) * ((float(windowHeight) / windowWidth)));

				// Clamp the output colour to 0-1 range before conversion.
				// Convert from linear space to sRGB.
				output.toSRGB();
				output.saturate();
				// Write the colour to the image (scaling up by 255).
				*image.data(x, row, 0, 0) = output.r * 255;
				*image.data(x, row, 0, 1) = output.g * 255;
				*image.data(x, row, 0, 2) = output.b * 255;
			}

			if (progressiveDisplay)
			{
				if (row % progressiveCount == 0)
				{
					main_disp.display(image);
				}
			}
		}		
	}
	return;
}

int main(int argc, char **argv)
{
	srand(0);
	kf::Time timer;
	
	std::string startupScript = "scene.lua";
	if (argc > 1)
	{
		startupScript = argv[1];
	}

	initLua(startupScript);

	int procCount;  //This is how many CPU threads that are available
	procCount = Poco::Environment::processorCount();

	//Test Code
	//std::cout << procCount << std::endl;
	//End test code
	
	//MOVED CODE
	// The floating point image target that the scene is rendered into.
	image.resize(windowWidth, windowHeight, 1, 3, 0);
	main_disp.display(image);

	// The display object used to show the image.
	//CImgDisplay main_disp(image, "Raytrace");
	main_disp.set_normalization(0);	// Normalisation 0 disables auto normalisation of the image (scales to make the darkest to brightest colour fit 0 to 1 range.

	// Record the starting time.
	long long startTime = timer.getTicks();

	bool running = true;
	std::vector<Poco::Thread*> threadPool;

	for (int i = 0; i < procCount; i++)
	{
		threadPool.push_back(new Poco::Thread());
	}
	std::cout << "ThreadPool No: " << threadPool.size() << std::endl;

	for each (Poco::Thread *thread in threadPool)
	{
		thread->start(RayTrace, (void*) windowHeight);

		//thread->join();
	}

	for each (Poco::Thread * thread in threadPool)
	{
		thread->join();
	}
	
	// Record ending time.
	long long endTime = timer.getTicks();

	//std::cout << "Threads Run: " << threadsRun << std::endl;

	// Save the output to a bmp file.
	image.save_bmp("output.bmp");

	// Display elapsed time in the window title bar.
	main_disp.set_title("Completed render time: %fs", timer.ticksToSeconds(endTime - startTime));
	main_disp.display(image);

	// Keep refreshing the window until it is closed or escape is hit.
	while (!main_disp.is_closed())
	{
		if (main_disp.is_keyESC())
			return 0;
		main_disp.wait();
	}

	return 0;
}



