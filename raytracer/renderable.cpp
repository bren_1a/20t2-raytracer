#include "renderable.h"
#include <algorithm>

void HitPoint::nearest(const HitPoint &hp)
{
	if (!m_hit)
		*this = hp;
	else
	{
		if (hp.m_hit && hp.m_distance < m_distance)
		{
			*this = hp;
		}
	}
}

Renderable::Renderable()
{
}

Sphere::Sphere(float rad) :m_radius(rad)
{
}

HitPoint Sphere::intersect(const kf::Ray &ray)
{
	HitPoint hp;
	hp.m_hit = ray.intersectSphere(m_position, m_radius, hp.m_distance);
	if (hp.m_hit)
	{
		hp.m_position = ray.interpolate(hp.m_distance);
		hp.m_renderable = this;
		hp.m_normal = normalise(hp.m_position - m_position);
	}
	return hp;
}

Box::Box(float w, float h, float d) : m_size(w,h,d)
{
}

HitPoint Box::intersect(const kf::Ray &ray)
{
	HitPoint hp;
	hp.m_hit = ray.intersectBox(m_position, m_size, hp.m_distance);
	if (hp.m_hit)
	{
		hp.m_position = ray.interpolate(hp.m_distance);
		hp.m_renderable = this;
		kf::Vector3 p = ((hp.m_position - m_position) / (m_size * 0.5)) * 1.00001f;
		if (abs(p.x) >= 1)
			hp.m_normal.set(p.x, 0, 0);
		else if (abs(p.y) >= 1)
			hp.m_normal.set(0, p.y, 0);
		else
			hp.m_normal.set(0, 0, p.z);
	}

	return hp;
}

